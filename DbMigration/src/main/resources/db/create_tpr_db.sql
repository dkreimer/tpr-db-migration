
/****** Drop the TPR database if it exists ******/
USE [master]
GO
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'tpr')
BEGIN
  ALTER DATABASE [tpr] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
  DROP DATABASE [tpr]
END
GO

/****** Do filestream configuration ******/
-- This option will show/allow advanced options
EXEC sp_configure 'show advanced options', 1;
-- Enable filestreams for SQL Server instance (Level 2 is the full filestream access including WIN32 API)
EXEC sp_configure filestream_access_level, 2;
-- Apply configuration
RECONFIGURE WITH OVERRIDE;
-- Show configured level to verify
EXEC sp_configure filestream_access_level;
-- Reset advanced options to 0
EXEC sp_configure 'show advanced options', 0;
GO

/****** Object:  Database [tpr]    Script Date: 10/27/2015 15:42:39 ******/
CREATE DATABASE [tpr] ON  PRIMARY 
( NAME = N'tpr', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\tpr.mdf' , SIZE = 1319488KB , MAXSIZE = UNLIMITED, FILEGROWTH = 25%),
 FILEGROUP [ereg_folder_item_document_filestream_group] CONTAINS FILESTREAM 
( NAME = N'ereg_folder_item_document_filestream_data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\DEV_filestream_data\ereg_folder_item_document_filestream_data'), 
 FILEGROUP [ereg_folder_item_filestream_group] CONTAINS FILESTREAM  DEFAULT 
( NAME = N'ereg_folder_item_filestream_data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\DEV_filestream_data\ereg_folder_item_filestream_data'),
 FILEGROUP [FILESTREAM] CONTAINS FILESTREAM
( NAME = N'Knowledgebase_filestream', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\DEV_filestream_data\SurveyBooks_DEV_FS' ),
 FILEGROUP [PDS_memorandum_files] CONTAINS FILESTREAM 
( NAME = N'SurveyBooks_PDS_memorandum_files', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\DEV_filestream_data\SurveyBooks_PDS_memorandum_files' )
LOG ON 
( NAME = N'tpr_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\tpr_log.ldf' , SIZE = 136064KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [tpr] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [tpr].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [tpr] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [tpr] SET ANSI_NULLS OFF
GO
ALTER DATABASE [tpr] SET ANSI_PADDING ON
GO
ALTER DATABASE [tpr] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [tpr] SET ARITHABORT OFF
GO
ALTER DATABASE [tpr] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [tpr] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [tpr] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [tpr] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [tpr] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [tpr] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [tpr] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [tpr] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [tpr] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [tpr] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [tpr] SET  DISABLE_BROKER
GO
ALTER DATABASE [tpr] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [tpr] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [tpr] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [tpr] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [tpr] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [tpr] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [tpr] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [tpr] SET  READ_WRITE
GO
ALTER DATABASE [tpr] SET RECOVERY FULL
GO
ALTER DATABASE [tpr] SET  MULTI_USER
GO
ALTER DATABASE [tpr] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [tpr] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'tpr', N'ON'
GO
